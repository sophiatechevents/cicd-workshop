## Installation des dépendances

```
$ bundle install
```

## Lancement du serveur

```
$ ruby app.rb -s Puma
```

## Test

```
$ curl localhost:8000
Hola!
```
