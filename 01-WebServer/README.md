Using the language of your choice, develop a web server with the following caracteristics:
- listen on port 8000
- expose the */* endpoint in GET
- return the string 'Hi!' for each request

Note: you can also use one of the template available in the current folder:
- Go
- Java
- NodeJs
- Python
- Ruby
- .net core

To use the template, clone this repository and copy the desired folder in your home directory. (copy the whole folder: there are some hidden files)

You can rename it to more friendly name.

[Let's add a Dockerfile and package the application](../02-Docker)
