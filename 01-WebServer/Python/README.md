## Installation de flask

```
$ pip install -r requirements.txt
```

## Lancement du serveur

```
$ python app.py
```

## Test

```
$ curl localhost:8000
Hello World!
```
