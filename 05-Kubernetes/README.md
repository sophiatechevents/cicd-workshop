## Add a Kubernetes cluster to your repository

### 1. Get the node's information

An instructor should provide each participant the ID of a virtual machine to be used in the next part of the workshop.

Shout it out load if you were not provided those information yet !!!

From the URL [cicd-workshop.serveo.net](https://cicd-workshop.serveo.net):
- get the IP Address corresponding to the ID of your node
- save the `key.pem` file in your home directory and give it the 400 mode (`chmod 400 $HOME/key.pem`)

Run a shell on the remote machine using the following command (you will first need to replace NODE_IP_ADDRESS with the IP address of your node)

```
$ ssh -i $HOME/key.pem root@NODE_IP_ADDRESS
```

### 2. Create a one node k3s cluster

We will now install [k3s.io](https://k3s.io), a light Kubernetes distribution coming from [Rancher](https://rancher.com/)

From the previous shell, run the following command:

```
$ curl -sfL https://get.k3s.io | sh -
```

Wait ~30 seconds and then check the status of the node

```
$ kubectl get node
```

> **Note**: `kubectl` is a binary used to communicate with a Kubernetes cluster, more on that in a bit

You should get something like the following

```
NAME      STATUS   ROLES    AGE   VERSION
node01    Ready    master   24s   v1.15.4-k3s.1
```

Congratulation, you have a fully functional Kubernetes cluster (ok, it's just a 1 node cluster but still...).

You can now close the ssh connection.

### 3. Configure your local machine to communicate with your cluster

To communicate with your cluster from your local machine, you need 2 things:
- the `kubectl` binary
- the configuration file of the cluster

#### 3.1. Install kubectl

`kubectl` is a binary that needs to be downloaded and added to the $PATH

- on macOS:

```
$ curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl
$ chmod +x ./kubectl
$ sudo mv ./kubectl /usr/local/bin/kubectl
```

- on Linux

```
$ curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
$ chmod +x ./kubectl
$ sudo mv ./kubectl /usr/local/bin/kubectl
```

- on Windows

```
$ curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.0/bin/windows/amd64/kubectl.exe
```

#### 3.2. Get the cluster's configuration

Get the configuration file (a.k.a kubeconfig). This one was created on `/etc/rancher/k3s/k3s.yaml` on your cluster's node.

```
$ scp -i $HOME/key.pem root@NODE_IP_ADDRESS:/etc/rancher/k3s/k3s.yaml k3s.yaml
```

Open the configuration file and change the IP of the server key with the external IP of your node:

Example:

```
server: https://127.0.0.1:6443
```

==>

```
server: https://167.99.86.26:6443
```

#### 3.3. Configure kubectl

Set the KUBECONFIG environment variable so it references the cluster's configuration file.

```
$ export KUBECONFIG=$PWD/k3s.yaml
```

You should now be able to get the information of your node through kubectl:

```
$ kubectl get nodes
NAME      STATUS   ROLES    AGE   VERSION
node01    Ready    master   21m   v1.15.4-k3s.1
```

### 4. Link the cluster to your GitLab repository

From the *Operations > Kubernetes* menu, select the *Add existing cluster* tab. 

![Existing Cluster](./images/add_cluster.png)

Copy the `setup.sh` script on your machine (this script is in the same folder as the README your are currently following) and run it from a terminal where you exported the KUBECONFIG environment variable beforehand (this is a very important step, ask an instructor if you need some help).

You will then obtain all the information needed to integrate your cluster in the project:
- cluster's name
- URL of the API Server
- cluster's CA
- user account

Add all those information in the corresponding fields

> **Note** : uncheck *GitLab-managed cluster*

Once you validate the form, your GitLab project will be able to use the Kubernetes cluster running on your node.

### 5. Add the Deployment and Service resources

Copy the following content into a `deploy.yml` file at the root of your application's folder.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: www
  labels:
    app: www
spec:
  selector:
    matchLabels:
      app: www  
  replicas: 2
  template:
    metadata:
      labels:
        app: www
    spec:
      containers:
      - name: www
        image: registry.gitlab.com/sophiatechevents/REPOSITORY
```

Replace *REPOSITORY* with your actual repository identifier.

Copy the following content into a `service.yml` file

```
apiVersion: v1
kind: Service
metadata:
  name: www
spec:
  type: NodePort
  ports:
    - name: www
      nodePort: 31000
      port: 80
      targetPort: 8000
      protocol: TCP
  selector:
    app: www
```

Run the following commande to create the Deployment and Service resources

```
$ kubectl apply -f deploy.yml
$ kubectl apply -f service.yml
```

After a couple of seconds, make sure the resources were correctly deployed with the following command:

```
$ kubectl get deploy,pod,svc
```

You should get an output similar to the following one:

```
NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.extensions/www   2/2     2            2           95s

NAME                       READY   STATUS    RESTARTS   AGE
pod/www-5fc78b8bb4-fv2jq   1/1     Running   0          95s
pod/www-5fc78b8bb4-6grn5   1/1     Running   0          95s

NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
service/kubernetes   ClusterIP   10.43.0.1       <none>        443/TCP        23m
service/www          NodePort    10.43.153.236   <none>        80:31000/TCP   89s
```

Using the IP adress the node of your cluster, check if your web server is available on *http://HOST_IP:31000*

Note: the external IP of your node can be obtained from the following command:

```
$ kubectl get node -o wide
```

You should get a result similar to the following one (the IP address will be different though)

```
NAME     STATUS   ROLES    AGE   VERSION         INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
node01   Ready    master   24m   v1.15.4-k3s.1   206.189.125.129   <none>        Ubuntu 18.04.3 LTS   4.15.0-58-generic   containerd://1.2.8-k3s.1
```

Send a request from the command line and verify your server is accessible.

```
$ curl http://206.189.125.129:31000
Hello World!
```

### 6. Setup the automated deployment step

In your *.gitlab-ci.yml* file, replace the step `deploy staging` by this one :

```
deploy kube:
  stage: deploy
  environment: staging
  image: lucj/kubectl:1.16.2
  script:
    - kubectl config set-cluster my-cluster --server=${KUBE_URL} --certificate-authority="${KUBE_CA_PEM_FILE}"
    - kubectl config set-credentials admin --token=${KUBE_TOKEN}
    - kubectl config set-context my-context --cluster=my-cluster --user=admin --namespace default
    - kubectl config use-context my-context
    - kubectl apply -f deploy.yml
  only:
    kubernetes: active
```

This step first create a kube config entry and use it to update the Deployment created previously.

Note: the image used in this step (**lucj/kubectl:1.16.2**) only contains the *kubectl* binary we need to setup a Kubernetes context and to update the application.

Make some changes to your application code:
- modify the string returned
- change the integration test accordingly

Then commit and push the changes.

Once the pipeline is done, make sure the application is updated by sending a new HTTP request.

### 7. Manually deploy to the production environment

Create  a `production` environment in GitLab.

![production](./images/env_production_1.png)

![production](./images/env_production_2.png)

Add the following step to `.gitlab-ci.yml`

```
deploy kube on production:
  stage: deploy
  environment: production
  image: lucj/kubectl:1.16.2
  script:
    - kubectl config set-cluster my-cluster --server=${KUBE_URL} --certificate-authority="${KUBE_CA_PEM_FILE}"
    - kubectl config set-credentials admin --token=${KUBE_TOKEN}
    - kubectl config set-context my-context --cluster=my-cluster --user=admin --namespace default
    - kubectl config use-context my-context
    - kubectl apply -f deploy.yml
  when: manual
  only:
    kubernetes: active
```

Commit and push the changes.

When the pipeline is finished you can manualy deploy to production.

![manual-deploy](./images/manual_deploy.png)
