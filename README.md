## Agenda

Discover how to setup a continuous integration with Gitlab CI and Docker and deploy your application in a Kubernetes cluster

## Who are we

* Luc Juggery, Traxxs
* Emeric Chardiny, ECY Conseil

## Prerequisites

A laptop with:
- Docker, you can get it from the [Docker Hub](https://hub.docker.com)
- Git

An account on https://gitlab.com, your public key deployed. 

## Steps

[1. Build a web server using your favorite language](./01-WebServer)  

[2. Add a Dockerfile](./02-Docker)  

[3. Create a Gitlab repository](./03-Gitlab)  

[4. Add a CICD pipeline](./04-CI)  

[5. Deploy on a Kubernetes cluster](./05-Kubernetes)  

[Let's start](./01-WebServer)
